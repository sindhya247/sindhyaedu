<!--header-top start here-->
<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-social wow bounceInLeft" data-wow-delay="0.3s">
			    <ul>
			    	<li><h5>Follow Us :</h5></li>
			    	<li><a href="#"><span class="fb"> </span></a></li>
			    	<li><a href="#"><span class="tw"> </span></a></li>
			    	<li><a href="#"><span class="in"> </span></a></li>
			    	<li><a href="#"><span class="gmail"> </span></a></li>
			    </ul>
			</div>
			<div class="col-md-8 header-address wow bounceInRight" data-wow-delay="0.3s">
				<ul>
					<li><span class="phone"> </span> <h6>+91 9788842111</h6></li>
					<li><span class="email"> </span><h6><a href="mailto:info@example.com">sindhyaacademy@gmail.com</a></h6></li>
					
				
				</ul>
			</div>
		  <div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--header-top end here-->
<!--header start here-->
	<!-- NAVBAR
		================================================== -->
	<style>
@media (min-width: 200px) and (max-width: 600px) {
 .logo1 {
	width: 240px;
    height: 76px;
    margin-left: 18%;
    margin-top: -9%;
}
 .name {
	 margin-top: 95px;
}
 .name1 {
	   font-size: 38px;
    color: rgb(107, 188, 152);
    font-style: inherit;
    font-weight: bold;
    font-family: serif;
    text-shadow: none;
    margin-left: -10px;
   
}
 .subname {
font-size: 11px;
    color: rgb(132, 138, 135);
    font-style: inherit;
    font-weight: bold;
    font-family: serif;
    text-shadow: none;
    margin-left: -9px;
 }
  
}
@media (min-width: 601px) and (max-width: 1600px) {
 .logo1 {		
	width: 100%;
    height: 109px;
    margin-top: -23px;	
   }
    .name {
	 margin-top: 0px;
}


 .name1 {
	  font-size: 44px;
    color: rgb(107, 188, 152);
    font-style: inherit;
    font-weight: bold;
    font-family: serif;
    text-shadow: none;  margin-left: 142px;
   
}
 .subname {
	 font-size: 14px;
    color: rgb(132, 138, 135);
    font-style: inherit;
    font-weight: bold;
    font-family: serif;
    text-shadow: none;
        margin-left: 426px;
   
}


}




</style>
 
<div class="header">
	<div class="fixed-header">	

		    <div class="navbar-wrapper">
		      <div class="container">
		        <nav class="navbar navbar-inverse navbar-static-top">
		             <div class="navbar-header">
			              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			                <span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			              </button>
			              <div class="logo wow slideInLeft" data-wow-delay="0.3s">
			                    <a class="navbar-brand " href="index.html"><img src="images/logo.png" class="logo1"style="" /></a>
			              </div>
			              
			              
						  			          <!--    <div class="logo wow slideInLeft name" data-wow-delay="0.3s">
						  
						  <font class="name1"style=" ">SINDHYA GROUP OF INSTITUTION<br></font>

	<font class="subname" style=" ">Affiliated to the CBSE (Central Board of Secondary Education)</font>
						  
						  <br>
						  
						  
						  </div> -->
						  
						 
			          </div>
			
		            <div id="navbar" class="navbar-collapse collapse" style="    margin-top: 108px;">
		            <nav class="cl-effect-16" id="cl-effect-16">
		              <ul class="nav navbar-nav">
		               <li><a class="active" href="index.php" data-hover="Home">Home</a></li>
					    						    <li class="dropdown"><a href="" class="dropdown-toggle" data-hover="Profile" data-toggle="dropdown"data-hover="Home">Profile<i class="fa fa-angle-down"></i></a>
						<ul class="navcolor dropdown-menu"  >
                          <!--        <li ><a href="about.php" class="navcolor">About Our School</a></li>
                              <li><a href="pmsg.php" class="navcolor">Principal’s Message</a></li> -->
                                <li><a href="#" class="navcolor">Vision & Mission</a></li>
                          
							
								
                            </ul></li>
						
										<li><a href="#" data-hover="Courses">Courses</a></li>	
						<!--   <li class="dropdown"><a href="" class="dropdown-toggle" data-hover="Curriculam" data-toggle="dropdown">Courses<i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu" >
                                <li ><a href="curriculam.php" class="navcolor">Our Curriculam</a></li>
                                <li><a href="calender.php" class="navcolor">School Calender</a></li>
                                <li><a href="uniform.php" class="navcolor">School Uniform</a></li>
                                <li><a href="extra-curricular.php" class="navcolor">Extra-curricular Activity</a></li>
                                <li><a href="co-curricular.php" class="navcolor">co-curricular Activity</a></li>
                     </ul> 
					 </li>  
					 -->
					 <li class="dropdown"><a href="" class="dropdown-toggle" data-hover="Facilities" data-toggle="dropdown">Facilities<i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu" >
                                <li ><a href="#" class="navcolor">Industry</a></li>
                               <li ><a href="#" class="navcolor">Culinary Lab</a></li>
                               <li ><a href="#" class="navcolor">Practical Classes</a></li>
                               <li ><a href="#" class="navcolor">Housekeeping Lab</a></li>
                               <li ><a href="#" class="navcolor">Bakery Lab</a></li>
                               <li ><a href="#" class="navcolor">Front Office</a></li>
                               <li ><a href="#" class="navcolor">Carving Class</a></li>
                               <li ><a href="#" class="navcolor">Restaurant Lab</a></li>
                               <li ><a href="#" class="navcolor">Bartending Lab</a></li>
                               <li ><a href="#" class="navcolor">Transport</a></li>
                               <li ><a href="#" class="navcolor">Students&#8217; Hostel</a></li>
                     </ul></li> 
					 <!--
					 <li class="dropdown"><a href="" class="dropdown-toggle" data-hover="Laboratories" data-toggle="dropdown">Laboratories<i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu" >
                     <li ><a href="language.php" class="navcolor">language Lab</a></li>
                     <li ><a href="computer.php" class="navcolor">Computer Lab</a></li>
                     <li ><a href="physics.php" class="navcolor">Physics Lab</a></li>
                     <li ><a href="chemistry.php" class="navcolor">Chemistry Lab</a></li>
                     <li ><a href="biology.php" class="navcolor">Biology Lab</a></li>
                     <li ><a href="math.php" class="navcolor">Math Lab</a></li>
                    
					</ul></li> -->
						<li><a href="#" data-hover="Online Fees Payment">Online Fees Payment</a></li>		
			<li class="dropdown"><a href="" class="dropdown-toggle" data-hover="Gallery" data-toggle="dropdown">Gallery<i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu" >
                                <li ><a href="gallery.php" class="navcolor">Photo</a></li>
                                <li><a href="video.php" class="navcolor">Video</a></li>
                           </ul></li>
						<li><a href="achievement.php" data-hover="Our Recruiters">Our Recruiters</a></li>
						<li><a href="admission.php" data-hover="#">Admission</a></li>
					
						<li><a href="contact.php" data-hover="Contact">Contact</a></li>				
		              </ul>
		            </nav>

		            </div>
		            <div class="clearfix"> </div>
		             </nav>
		          </div>
		           <div class="clearfix"> </div>
		    </div>
	 </div>
</div>
<!--header end here-->