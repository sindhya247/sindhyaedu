<!DOCTYPE HTML>
<html>
<head>
<title>Sindhya Institute</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Hind:400,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--google fonts-->
<!-- animated-css -->
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
<!-- animated-css -->
<script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php include 'nav.php'; ?>


<!--about start here-->
<div class="about">
	<div class="container">
		<div class="about-main">
		
			
			<div class="about-bottom">
				<div class="col-md-5 about-left wow bounceInLeft" data-wow-delay="0.3s">
					<img src="images/lab.jpg" alt="" class="img-responsive">
				</div>
				<div class="col-md-7 about-right wow bounceInRight" data-wow-delay="0.3s">
						<div class="about-top" style="    margin-bottom: 0em;">
					  		<h1>VISION</h1></div>
					  		<p>    To be an institution of excellence providing education of global Standard to all sections of the society at an affordable cost and to parallel the Process of Professional Education with ethical values shaping the career of the student as an individual to excel globally.

</p>
					<div class="about-top" style="    margin-bottom: 0em;">
				 <h1>MISSION</h1></div>
					  		<p>To set a benchmark of excellence in the teaching-learning process.<br>
To create excellence in various perspectives, dimensions, and domains through education.<br>
To foster a passion for learning and creative thinking.<br>
To prepare management professionals across various verticals with Global Mindset<br>
		</p>
				
				</div> 
			  <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!--about end here-->
<!--about advantages start here-->

<!--about advantages end here--><script src="js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.gallery-grid a').Chocolat();
		});
		</script>
<!--gallery end here-->
<?php include 'footer.php'; ?>

</body>
</html>