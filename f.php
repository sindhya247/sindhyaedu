<!--services strip start here-->
<div class="ser-strip">
	<div class="container">
		<div class="ser-strip-main wow rollIn" data-wow-delay="0.3s">
			   <h2>Thought For The Day</h2>
			   <p>The highest education is that which does not merely give us information but makes our life in harmony with all existence.
- Rabindranath Tagore</p>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--services strip end here-->
<!--features strat here-->
<div class="features">
	<div class="container">
		<div class="features-main">
			<div class="features-top wow slideInLeft" data-wow-delay="0.3s">
				<h3>Why Sindhya?</h3>
				<h4>This new breed school believes that school for modern education 
				should provide wide range of facilities to offer to facilitate extensive learning.  These include the following features:</h4>
			</div>
			<div class="features-bottom wow slideInRight" data-wow-delay="0.3s">
				<div class="col-md-3 featur-grid">
					<h4>Wide Employment Opportunity</h4>
					<ul>
					         <li ><a href="#" >Class Room</a></li>
                               <li ><a href="#" >Smart Class</a></li>
                               <li ><a href="#" >Auditorium</a></li>
                               <li ><a href="#" >Library</a></li>
                               <li ><a href="#" >Swimming Pool</a></li>
                               <li ><a href="#" >Dining Hall</a></li>
					</ul>
				</div>
				<!-- <div class="col-md-3 featur-grid">
					<h4>Skills Development</h4>
					<ul>
					    <li ><a href="#" >language Lab</a></li>
                     <li ><a href="#" >Computer Lab</a></li>
                     <li ><a href="#" >Physics Lab</a></li>
                     <li ><a href="#" >Chemistry Lab</a></li>
                     <li ><a href="#" >Biology Lab</a></li>
                     <li ><a href="#" >Math Lab</a></li>
					</ul>
				</div> -->
				<div class="col-md-3 featur-grid">
					<h4>Work across the Globe</h4>
					<ul>
						<li><a href="#">Canada</a></li>
						<li><a href="#">Singapore</a></li>
						<li><a href="#">Malaysia</a></li>
						<li><a href="#">Maldevis</a></li>
						<li><a href="#">Mumbai</a></li>
						<li><a href="#">Delhi </a></li>
					
					</ul>
				</div>
				<div class="col-md-3 featur-grid">
					<h4>Diverse Industry Placements</h4>
					<ul>
					<li><a href="#">Star Hotels</a></li>
	<li><a href="#">Luxury Resorts</a></li>
	<li><a href="#">Airlines</a></li>
	<li><a href="#">Cruise ships</a></li>
	<li><a href="#">Corporates Chains</a></li>
	<li><a href="#">& Restaurant Chains</a></li>

					
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!--features end here-->