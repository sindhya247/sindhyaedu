<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<?php include 'head.php'; ?>
<body>
<?php include 'nav.php'; ?>
<div class="about-top">
				<h1>Our Class Room

</h1>
			</div>
<!--single start here-->
<div class="single">
	<div class="container">
		<div class="single-top wow bounceInLeft" data-wow-delay="0.3s">
			<img class="img-responsive wow fadeInUp animated" data-wow-delay=".5s" src="images/class1.jpg" alt="" />
				<div class="lone-line">
					<h1>Class Room</h1>	<br>
				
					
						<p class="wow fadeInLeft animated" data-wow-delay=".5s">Classrooms are spacious, well ventilated and equipped with computer and audiovisual equipment, allowing the teachers to teach using a wide variety of media.
All classrooms meet and exceed CBSE norms of space per student with excellent furniture and teaching aids.</span></p>
				</div>
		</div>


	</div>		
</div>
<br>
<!--single end here-->
<?php include 'f.php'; ?>

<?php include 'footer.php'; ?>

</body>
</html>