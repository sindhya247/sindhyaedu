<!DOCTYPE HTML>
<html>
    <style> 
@font-face
{
    font-family: myTamilFont;
    src: url(tamil_font.ttf);
}

div
{
    font-family: myTamilFont;
}
</style>
<?php include 'head.php'; ?>
<body>
<?php include 'nav.php'; ?>
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script type="text/javascript">
    function Validate() {
     
		
		  var x = document.forms["myForm"]["student_name"].value;
    if (x == "") {
        alert("Name must be filled out");
        return false;
    }  var x = document.forms["myForm"]["student_email"].value;
    if (x == "") {
        alert("Email must be filled out");
        return false;
    }  var x = document.forms["myForm"]["student_phone"].value;
    if (x == "") {
        alert("Phone Number must be filled out");
        return false;
    }  var x = document.forms["myForm"]["student_school"].value;
    if (x == "") {
        alert("Student School Name  must be filled out");
        return false;
    } 
		
		
        return true;
    }
</script>
<div class="about-top">
				<h1>Registration Form

</h1>
			</div>
<!--single start here-->
<div class="courses_box1">
	   <div class="container">
	   	  <form class="login" method='POST'  name="myForm" action="register_form.php">
		  <table align="center" cellpadding = "10">
 
<!----- First Name ---------------------------------------------------------->
<tr>
<td>மாணவன் /மாணவி பெயர் 
</td><td><input type="text" name="First_Name" maxlength="30"/>
(max 30 characters a-z and A-Z)
</td>
</tr>
 
<!----- father Name ---------------------------------------------------------->
<tr>
<td>தந்தை பெயர் </td>
<td><input type="text" name="Father_Name" maxlength="30"/>
(max 30 characters a-z and A-Z)
</td>
</tr>
 <!----- Last Name ---------------------------------------------------------->
<tr>
<td>தாயின் பெயர் </td>
<td><input type="text" name="Mother_Name" maxlength="30"/>
(max 30 characters a-z and A-Z)
</td>
</tr>
 
<!----- Date Of Birth -------------------------------------------------------->
<tr>
<td>பிறந்த தேதி </td>
 
<td>
<select name="Birthday_Day" id="Birthday_Day">
<option value="-1">Day:</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
 
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
 
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
 
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
 
<option value="31">31</option>
</select>
 
<select id="Birthday_Month" name="Birthday_Month">
<option value="-1">Month:</option>
<option value="January">Jan</option>
<option value="February">Feb</option>
<option value="March">Mar</option>
<option value="April">Apr</option>
<option value="May">May</option>
<option value="June">Jun</option>
<option value="July">Jul</option>
<option value="August">Aug</option>
<option value="September">Sep</option>
<option value="October">Oct</option>
<option value="November">Nov</option>
<option value="December">Dec</option>
</select>
 
<select name="Birthday_Year" id="Birthday_Year">
 
<option value="-1">Year:</option>
<option value="2012">2012</option>
<option value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
<option value="2007">2007</option>
<option value="2006">2006</option>
<option value="2005">2005</option>
<option value="2004">2004</option>
<option value="2003">2003</option>
<option value="2002">2002</option>
<option value="2001">2001</option>
<option value="2000">2000</option>
 
<option value="1999">1999</option>
<option value="1998">1998</option>
<option value="1997">1997</option>
<option value="1996">1996</option>
<option value="1995">1995</option>
<option value="1994">1994</option>
<option value="1993">1993</option>
<option value="1992">1992</option>
<option value="1991">1991</option>
<option value="1990">1990</option>
 
<option value="1989">1989</option>
<option value="1988">1988</option>
<option value="1987">1987</option>
<option value="1986">1986</option>
<option value="1985">1985</option>
<option value="1984">1984</option>
<option value="1983">1983</option>
<option value="1982">1982</option>
<option value="1981">1981</option>
<option value="1980">1980</option>
</select>
</td>
</tr>
 

 
<!----- Gender ----------------------------------------------------------->
<tr>
<td>பாலினம் </td>
<td>
ஆண்  <input type="radio" name="Gender" value="Male" />
பெண்  <input type="radio" name="Gender" value="Female" />
</td>
</tr>
 <!----- Caste ---------------------------------------------------------->
<tr>
<td>பிரிவு </td>
<td><input type="text" name="Caste" maxlength="" />

</td>
</tr>
 <!----- Blood Group ---------------------------------------------------------->
<tr>
<td>இரத்த வகை </td>
<td><input type="text" name="Blood_Group" maxlength="" />

</td>
</tr>
<!----- Address ---------------------------------------------------------->
<tr>
<td>வீட்டு முகவரி  <br /><br /><br /></td>
<td><textarea name="Address" rows="4" cols="30"></textarea></td>
</tr>
 
<!----- City ---------------------------------------------------------->
<tr>
<td>ஊர் </td>
<td><input type="text" name="City" maxlength="30" />
(max 30 characters a-z and A-Z)
</td>
</tr>
 
<!----- Pin Code ---------------------------------------------------------->
<tr>
<td>பின்க்கோடு </td>
<td><input type="text" name="Pin_Code" maxlength="6" />
(6 digit number)
</td>
</tr>
 <!----- Email Id ---------------------------------------------------------->
<tr>
<td>இ- மெயில் </td>
<td><input type="text" name="Email_Id" maxlength="100" /></td>
</tr>
 
<!----- Mobile Number ---------------------------------------------------------->
<tr>
<td>தொடர்பு எண் </td>
<td>
<input type="text" name="Mobile_Number" maxlength="10" />
(10 digit number)
</td>
</tr>
<!----- State ---------------------------------------------------------->
<!--<tr>
<td>STATE</td>
<td><input type="text" name="State" maxlength="30" />
(max 30 characters a-z and A-Z)
</td>
</tr> -->
 
<!----- Country ---------------------------------------------------------->
<!--<tr>
<td>COUNTRY</td>
<td><input type="text" name="Country" value="India" readonly="readonly" /></td>
</tr> -->
 <!----- School ---------------------------------------------------------->
<tr>
<td>பள்ளியின் பெயர் </td>
<td><input type="text" name="School"  /></td>
</tr>
 

 <!----- Qualification ---------------------------------------------------------->
<tr>
<td>தகுதி </td>
<td>
<select name="Qualification" id="Qualification">
<option name="Qualification" value="-1">தகுதி </option>
<option  name="Qualification" value="IX">IX</option>
<option name="Qualification" value="X">X</option>
<option name="Qualification" value="XI">XI</option>
<option  name="Qualification" value="XII">XII</option>
</td>
</tr>
 <!----- School ---------------------------------------------------------->
<tr>
<td>பெறப்பட்ட மதிப்பெண் </td>
<td><input type="text" name="Mark"  /></td>
</tr>
 <!----- Father Occupation ---------------------------------------------------------->
<tr>
<td>தந்தை தொழில் </td>
<td><input type="text" name="Father_Occup"  /></td>
</tr>
 <!----- Monthly Salary  ---------------------------------------------------------->
<tr>
<td>தந்தை மாத வருமானம் </td>
<td><input type="text" name="Father_Income"  /></td>
</tr>
 <!----- Mother Occupation ---------------------------------------------------------->
<tr>
<td>தாயின் தொழில் </td>
<td><input type="text" name="Mother_Occup"  /></td>
</tr>
 <!----- Monthly Salary ---------------------------------------------------------->
<tr>
<td>தாயின் மாத வருமானம் </td>
<td><input type="text" name="Mother_Income"  /></td>
</tr>

<!----- Course ---------------------------------------------------------->
<tr>
<td>COURSES<br />APPLIED FOR</td>
<td>
 
<input type="radio" name="Course" value="1">Diplamo in Hotel Management and Tourism<br>
<input type="radio" name="Course" value="4">Diplamo in Hotel Management and catering<br>
<input type="radio" name="Course" value="2">Diplamo in Hospital Management<br>

<input type="radio" name="Course" value="3">Diplamo in Health Care Assistant<br>


</td>
</tr>
 
<!----- Submit and Reset ------------------------------------------------->
<tr>
<td colspan="2" align="center">
<input type="submit" id="submit" value="Register" name="submit" onclick="return Validate()>
<input type="reset" value="Reset">
</td>
</tr>
</table>
                
		
   
            </form>
	   </div>
	</div>
<div class="about aboutdiv" style="
    margin-top: -34px;">
	<div class="container aboutdiv" style="">
		<div class="about-main">
		
			<div class="about-bottom">
			
			
		</div>
	</div>
</div>
<br>
<!--single end here-->
<?php include 'f.php'; ?>

<?php include 'footer.php'; ?>

</body>
</html>