<?php

include("db.php");

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Sindhya International School</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Hind:400,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--google fonts-->
<!-- animated-css -->
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
<!-- animated-css -->
<script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php include 'nav.php'; ?>


<!--gallery start here-->
<div class="gallery" id="gallery">
	<div class="container">
	  <div class="gallery-main wow zoomIn" data-wow-delay="0.3s">
	  	<div class="gallery-top">
	  		<h1>Gallery</h1>
	  	</div>
		            
		<div class="gallery-bott">
	     	<?php
	$query=mysql_query("select * from gallery_heading")or die(mysql_error());
	 while($row=mysql_fetch_array($query)){
	$id=$row['heading_id'];
	$heading_id=$row['heading_id'];
	
	?>
		<a href="gallery_details.php?heading_id=<?php echo $heading_id ?>"  >
			<div class="col-md-4 col1 gallery-grid ">

				<figure class="effect-bubba">
							<img class="img-responsive" src="admin/gallery/<?php echo $row['heading_photo']; ?>" alt="" style="    width: 350px;
    height: 232px;">
							<figcaption>
								<h4 class="gal"><?php echo $row['heading_title']; ?></h4>
								<p class="gal1"></p>	
							</figcaption>			
						</figure>
					<h4 class="gal"><?php echo $row['heading_title']; ?></h4>
						</div></a>
	
	<?php
	 }
	 ?>
			     <div class="clearfix"> </div>	
			</div>				
		</div>
	</div>
</div>
<script src="js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.gallery-grid a').Chocolat();
		});
		</script>
<!--gallery end here-->
<?php include 'footer.php'; ?>

</body>
</html>