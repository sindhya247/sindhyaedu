
<!DOCTYPE HTML>
<html>
<head>
<title>Sindhya Group Of Institution</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Hind:400,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--google fonts-->
<!-- animated-css -->
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
<!-- animated-css -->
<script src="js/bootstrap.min.js"></script>
</head>
<body>

<!--header-top end here-->
<!--header start here-->
	<!-- NAVBAR
<?php include 'nav.php'; ?>

<!--header end here-->
<!--contact start here-->
<div class="contact">
	<div class="container">
		<div class="contact-main">
			<div class="contact-top">
				<h2>Contact</h2>
			</div>
			<div class="contact-bottom">
				<div class="col-md-9 contact-left">
					<form>
						<input type="text" placeholder="Name">
						<input type="text" class="email" placeholder="Email">
						<textarea  placeholder="Message" required=""></textarea>
						<input type="submit" value="Send">
					</form>
				</div>
				<div class="col-md-3 contact-right">
					<h1>Address</h1>
                    <p> M.M.Kovilur pirivu, near AKASHAYA SCHOOL,</p>
                    <p>Seelapadi,</p>
                    <p> Dindigul,</p>
                    <p>Tamilnadu.</p>

				<p>+91 9788842111</p>
					<p><a href="mailto:info@siohm.com">info@siohm.com</a></p>
				</div>
			  <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!--contact end here-->
<!--map start here-->
<div class="map">
  <div class="container">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d31093.86422834192!2d80.247082!3d13.052658!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4a4daa75057a4e32!2sSINDHYA+SOFTWARE+pvt+ltd!5e0!3m2!1sen!2sin!4v1505718010082" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
 </div>
</div>
<!--map end here-->
<!--footer start here-->
<?php include 'footer.php'; ?>

</body>
</html>