<?php include("db.php");

?>

<!DOCTYPE HTML>
<html>
<meta charset="utf-8"/>
<?php include 'head.php'; ?>
<body>
<?php include 'nav.php'; ?>
<?php include 'banner.php'; ?>
<style>
@media (min-width: 200px) and (max-width: 600px) {
 .aboutdiv {
	 width:100%;
	 
 }
}
@media (min-width: 601px) and (max-width: 1600px) {
 .aboutdiv {
	 width: 900px;
 }
}
	</style> 
<div class="about aboutdiv" style="
    margin-top: -34px;">
	<div class="container aboutdiv" style="">
		<div class="about-main">
			<div class="about-top">
				<h1>About Our Institution</h1>	
			</div>
			<div class="about-bottom">
				<div class="col-md-5 about-left wow bounceInLeft" data-wow-delay="0.3s">
					<img src="images/i1.jpg" alt="" class="img-responsive">
				</div>
				<div class="col-md-7 about-right wow bounceInRight" data-wow-delay="0.3s">
				
					<p>The horizons of the Indian hospitality industry continue to expand at a dynamic. 
					Today the hospitality industry contributes invaluably to the Indian economy by creating employment and 
					entrepreneurial opportunities and by being one of the highest grosser's of foreign exchange for the national treasury. 
					Looking at the pace of growth of the hospitality industry, it is envisaged that there will soon be a marked shortage of managerial 
					and technically skilled professionals. Keeping this in mind, our curriculum is designed to meet the challenges and demands of domestic 
					and global markets.
The aim of the Institute is to produce experts suited to all sectors of the hospitality industry including administration, planning and strategy development.</p>		    
<div class="about-list">
					 <!--   	<div class="about-img">
					  		 <span class="abou-icon1"> </span>
					  	</div>
						<div class="about-text">
					  		<h5>VISION</h5>
					  		<p>    To be an institution of excellence providing education of global Standard to all sections of the society at an affordable cost and to parallel the Process of Professional Education with ethical values shaping the career of the student as an individual to excel globally.

</p>
					  	</div> -->
					  	 <div class="clearfix"> </div>
				   </div>
				   <div class="about-list">
					  <!--	  	<div class="about-img">
					  		 <span class="abou-icon2"> </span>
					  	</div>
					<div class="about-text">
					  	   <h5>MISSION</h5>
					  		<p>To set a benchmark of excellence in the teaching-learning process.
To create excellence in various perspectives, dimensions, and domains through education.
To foster a passion for learning and creative thinking.
To prepare management professionals across various verticals with Global Mindset
		</p>
					  	</div> -->
					  	 <div class="clearfix"> </div>
				   </div>
				</div>
			  <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<?php include 'news.php'; ?>

		

<!--information start here-->
<div class="information" style="   ">
	<div class="container">
		<div class="information-main">
		<div class="about-top">
				<h1>Facilities</h1>
			</div>
			  <div class="information-grid five-star wow slideInLeft" data-wow-delay="0.3s">
			  	<div class="col-md-4 hotel-info">
			  		<div class="info-left">
			  			<img src="images/A1.png" style="width:60px;height:60px"alt="">
			  		</div>
			  		<div class="info-right">
			  			<h4>Class Room</h4>
			  	
			  		</div>
			  	  <div class="clearfix"> </div>
			  	</div>
			  	<div class="col-md-4 hotel-info">
			  		<div class="info-left">
			  			<img src="images/A2.png" alt="" style="width:60px;height:60px">
			  		</div>
			  		<div class="info-right">
			  			<h4>Practical Classes</h4>
			  		
			  		</div>
			  	  <div class="clearfix"> </div>
			  	</div>
			  	<div class="col-md-4 hotel-info">
			  		<div class="info-left">
			  			<img src="images/A3.png" alt="" style="width:60px;height:60px">
			  		</div>
			  		<div class="info-right">
			  			<h4>Bakery Lab</h4>
			  		
			  		</div>
			  	  <div class="clearfix"> </div>
			  	</div>
			  	<div class="clearfix"> </div>
			  </div>
			  <div class="information-grid wow slideInRight" data-wow-delay="0.3s">
			  	<div class="col-md-4 hotel-info">
			  		<div class="info-left">
			  			<img src="images/A4.png" alt="" style="width:60px;height:60px">
			  		</div>
			  		<div class="info-right">
			  			<h4>Carving Class</h4>
			  		
			  		</div>
			  	  <div class="clearfix"> </div>
			  	</div>
			  	<div class="col-md-4 hotel-info">
			  		<div class="info-left">
			  			<img src="images/A5.png" alt="" style="width:60px;height:60px">
			  		</div>
			  		<div class="info-right">
			  			<h4>Restaurant Lab</h4>
			  	
			  		</div>
			  	  <div class="clearfix"> </div>
			  	</div>
			  	<div class="col-md-4 hotel-info">
			  		<div class="info-left">
			  			<img src="images/A6.png" alt="" style="width:60px;height:60px">
			  		</div>
			  		<div class="info-right">
			  			<h4>Bartending Lab</h4>
			  	
			  		</div>
			  	  <div class="clearfix"> </div>
			  	</div>
			  	<div class="clearfix"> </div>
			  </div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--information end here-->
<!--homegrids start here-->
<!--
<div class="home-block">
	<div class="container">
		<div class="home-main">
			<div class="home-top">
				<h3>Latest  News</h3>
			</div>
			<div class="home-bottom  wow bounceInLeft" data-wow-delay="0.3s">
			
	
				<div class="col-md-4 home-grid" style="    width: 110.333333%;">
					<div class="item item-type-move" style="    height: 414px;">
					<?php include 'calender.php'; ?>
					</div>
				</div>
			  <div class="clearfix"> </div>
			</div>
		  <div class="clearfix"> </div>
		</div>
	</div>
</div> -->
<!--home grid end here-->
<!--branches start here-->
<div class="branches">
		<div class="branches-main wow zoomIn" data-wow-delay="0.3s">
			<div class="branches-top">
				<h3>Our Events</h3>
			</div>
			<div  id="effect-5" class="branch-btm">
			
		<?php
	$query=mysql_query("select * from gallery_heading")or die(mysql_error());
	 while($row=mysql_fetch_array($query)){
	$id=$row['heading_id'];
		$heading_id=$row['heading_id'];
	
	?>
				<div  class="col-md-3 branch-gd-main" >					
					<div class="branch-gd  no-mar"> <a href="gallery_details.php?heading_id=<?php echo $heading_id ?>"">
						<img src="admin/gallery/<?php echo $row['heading_photo']; ?>" style="    width: 350px; height: 232px;"alt="" class="img-responsive">
						<div class="overlay">
		                    <span class="expand lardge">+</span>
		                    <span class="close-overlay hidden"><?php echo $row['heading_photo']; ?></span>
	                  		 </div></a>
                    </div><h4 class="gal"><?php echo $row['heading_title']; ?></h4>                 
				</div>				
	 <?php } ?>
			</div>
		  <div class="clearfix"> </div>
		</div>
</div>
<!--branches end here-->


<!--leaves start here-->
<?php include 'f.php'; ?>
<!--leaves end here-->
<!--

<div class="branch-text">
   <div class="container">
			<span class="quotations"> </span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  </div>
</div>

<div class="swimming">
	<div class="container">
		<div class="swimming-main wow zoomIn" data-wow-delay="0.3s">
			<div class="swimming-top">
				<h3>Welcome</h3>
			</div>
			<div class="swimmimg-bot">
				<div class="col-md-3 swimming-grid">
					<h4>758</h4>
					<span class="swim-icon-1"> </span>
					<h5>Lorem Ipsum</h5>
				</div>
				<div class="col-md-3 swimming-grid">
					<h4>4,790</h4>
					<span class="swim-icon-2"> </span>
					<h5>Lorem Ipsum</h5>
				</div>
				<div class="col-md-3 swimming-grid">
					<h4>7,920</h4>
					<span class="swim-icon-3"> </span>
					<h5>Lorem Ipsum</h5>
				</div>
				<div class="col-md-3 swimming-grid">
					<h4>190</h4>
					<span class="swim-icon-4"> </span>
					<h5>Lorem Ipsum</h5>
				</div>
			  <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!--swimmg emd here-->


<?php include 'footer.php'; ?>
</body>
</html>