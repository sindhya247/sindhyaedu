<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<?php include 'head.php'; ?>
<body>
<?php include 'nav.php'; ?>
<div class="about-top">
				<h1>Our Smart Room</h1>
			</div>
<!--single start here-->
<div class="single">
	<div class="container">
		<div class="single-top wow bounceInLeft" data-wow-delay="0.3s">
			<img class="img-responsive wow fadeInUp animated" data-wow-delay=".5s" src="images/smartclass.jpg" alt="" />
				<div class="lone-line">
					<h1>Smart Room</h1>	<br>

					
						<p class="wow fadeInLeft animated" data-wow-delay=".5s">
						The Smart Class program is introduced in our school by Educomp which is powered by the world’s largest repository of digital content based on Indian Schools curriculum. Existing class rooms are converted into technology enabled smart class rooms equipped with smart class Interactive Digiboard systems with a PC, Connected to a dedicated Smart Class knowledge centre inside the school campus. Teachers use the digital resources such as animations, video clips and simulated models as a part of their 40 minute class room period to teach the specific topics in smart class rooms, enabling a month sensory learning experience for the students. A dedicated Program Administrator provides teachers with all technical help required to work with the program. All the teachers are given specific training required for the Smart Class which improves the teachers effectiveness and productivity. Smart Class makes learning an 
						enjoyable experience for students. There is fast improvement in the academic performance of the students.</span></p>
				</div>
		</div>
	
	
	</div>		
</div>
<br>
<!--single end here-->

<?php include 'f.php'; ?>
<?php include 'footer.php'; ?>

</body>
</html>