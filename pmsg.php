<!DOCTYPE HTML>
<html>
<head>
<title>Sindhya International School</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Hind:400,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--google fonts-->
<!-- animated-css -->
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
<!-- animated-css -->
<script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php include 'nav.php'; ?>


<!--about start here-->
<div class="about">
	<div class="container">
		<div class="about-main">
			<div class="about-top">
				<h1>Principal’s Message

</h1>
			</div>
			<div class="about-bottom">
				<div class="col-md-5 about-left wow bounceInLeft" data-wow-delay="0.3s">
					<img src="images/p1.png" alt="" class="img-responsive">
				</div>
				<div class="col-md-7 about-right wow bounceInRight" data-wow-delay="0.3s">
					<h4></h4>
			<p>At Sindhya International School we give our students permission to dream big things. Our Mission is to nurture individual potential.
<br>
We believe in cultivating intelligence and talent. Rather than being fixed and finite, intelligence and talent will expand and grow, given the right encouragement and ambient. So we aim to keep students’ dreams and aspirations alive.
 A High Quality Education is provided in our institution inspiring the students to reach nobler, exceptional and sublime heights in life.


<br>
</p>			</div>
			  <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!--about end here-->
<!--about advantages start here-->

<!--about advantages end here--><script src="js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.gallery-grid a').Chocolat();
		});
		</script>
<!--gallery end here-->
<?php include 'footer.php'; ?>

</body>
</html>